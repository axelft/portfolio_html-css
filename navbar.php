<header>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- DEBUT Titre site et logo menu mobile -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <a class="navbar-brand" href="index.php"><h1>Axel FT  <small>Portfolio</small></h1> </a>
            </div>
            <!-- FIN Titre site et logo menu mobile -->
            <!-- DEBUT liens menu -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class=""><a href="index.php">Présentation Personnelle<span class="sr-only">(current)</span></a></li>
                    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Parcours <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="parcours-professionnel.php">Professionnel</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="parcours-personnel.php">Personnel</a></li>
                    </ul>
                    </li>
                    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Fichiers/Liens <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a target="_blank" href="files/CV_Axel_Floquet-Trillot.pdf">CV (PDF)</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a target="_blank" href="http://aps-paris5.fr">Site de l'APS</a></li>
                    </ul>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="contact.php">Contact</a></li>
                </ul>
            </div>
            <!-- FIN Liens menu -->
        </div>
        <!-- FIN container-fluid -->
    </nav>
</header>

